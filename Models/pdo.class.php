<?php

class PDO_SAKILA {
    private ?PDO $pdo = null;

    const HOST = "127.0.0.1";
    const DB = "sakila";
    const USER = "root";
    const PASSWORD = "root";

    function __construct()
    {
        try {
            if($this->pdo == null) {
                $this->pdo = new PDO("mysql:host=".self::HOST.";dbname=".self::DB, self::USER, self::PASSWORD);
            }
        } catch (Exception $e) {
            die("Erreur __construct : ".$e->getMessage());
        }
    }

    function getInstance() {
        return $this->pdo;
    }
}