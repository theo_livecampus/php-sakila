<?php
require_once("../Models/pdo.class.php");

class FilmRepository {

    private $pdo = null;

    public function __construct()
    {
        $pdo = new PDO_SAKILA();
        $this->pdo = $pdo->getInstance();
    }

    function getAll() {
        $stmt = $this->pdo->prepare("SELECT * FROM film");
        $stmt->execute();
        $films = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $films;
    }

    function get($id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM film WHERE film_id = :id");
        $stmt->execute([":id" => $id]);
        $film = $stmt->fetch(PDO::FETCH_ASSOC);

        return $film;
    }
}