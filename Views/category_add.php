<?php

require_once("../Models/category.class.php");

$categoryRepository = new CategoryRepository();
$name = filter_input(INPUT_POST, "name");

if($name) {
    $categoryRepository->add($name);
    echo "ok";
};

$_title = "Ajouter une catégorie";

require_once("../Components/header.php");
require_once("../Components/navbar.html");
?>
<div class="flex justify-center py-5">
    <h1 class="text-3xl text-teal-700 font-bold">Ajouter une catégorie</h1>
</div>
<div class="flex flex-col items-center">
    <form class="px-8 pt-6 pb-8 w-96" method="POST">
        <div class="mb-4">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="name">
                Nom de la catégorie
            </label>
            <input required class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="name" id="name" type="text" placeholder="nom de la categorie">
        </div>
        <div class="flex items-center justify-between">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                Créer la catégorie
            </button>
        </div>
    </form>
</div>

<?php
require("../Components/footer.html");
