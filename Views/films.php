<?php

require_once("../Models/film.class.php");

$filmRepository = new FilmRepository();
$films = $filmRepository->getAll();

$_title = "Films";

require_once("../Components/header.php");
require_once("../Components/navbar.html");
?>
<div class="flex justify-center py-5">
    <h1 class="text-3xl text-teal-700 font-bold">Liste des films</h1>
</div>
<?php if (empty($films)) : ?>
    <p>Aucun film</p>
<?php else : ?>
    <table class="min-w-full divide-y divide-gray-200">
        <thead class="bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">#</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Titre</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Notes</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Dernière modification</th>
                <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Actions</span>
                </th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
            <?php foreach ($films as $film) : ?>
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["film_id"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["title"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["rating"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["last_update"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <a href="/Views/film.php?id=<?= $film["film_id"] ?>" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>


<?php
require("../Components/footer.html");
