<?php

require_once("../Models/category.class.php");

$categoryRepository = new CategoryRepository();
$categories = $categoryRepository->getAll();

$_title = "Catégories";

require_once("../Components/header.php");
require_once("../Components/navbar.html");
?>
<div class="flex justify-center py-5">
    <h1 class="text-3xl text-teal-700 font-bold">Liste des catégories</h1>
</div>
<a class="mx-10 bg-teal-500 text-gray-50 px-4 py-2 my-2 rounded cursor-pointer" href="/Views/category_add.php">Ajouter</a>
<?php if (empty($categories)) : ?>
    <p>Aucune catégorie</p>
<?php else : ?>
    <table class="min-w-full divide-y divide-gray-200">
        <thead class="bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">#</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Titre</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Dernière modification</th>
                <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Actions</span>
                </th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
            <?php foreach ($categories as $category) : ?>
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $category["category_id"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">

                        <?= $category["name"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $category["last_update"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium space-x-5">
                        <a href="/Views/category.php?id=<?= $category["category_id"] ?>" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                        <a href="/Views/category.php?id=<?= $category["category_id"] ?>" class="text-indigo-600 hover:text-indigo-900">Editer</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>


<?php
require("../Components/footer.html");
