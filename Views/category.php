<?php

require_once("../Models/category.class.php");

$categoryId = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);


$categoryRepository = new CategoryRepository();
$category = $categoryRepository->get($categoryId);
$films = $categoryRepository->getFilms($categoryId);

if(isset($_GET['remove'])) {
    $categoryRepository->remove($categoryId);
}

$_title = $category["name"];

require_once("../Components/header.php");
require_once("../Components/navbar.html");
?>
<div class="flex justify-center py-5">
    <h1 class="text-3xl text-teal-700 font-bold"><?= $category["name"] ?></h1>
</div>
<ul class="flex justify-center text-gray-500 space-x-5">
    <li>ID : <?= $category["category_id"] ?></li>
    <li>Last update : <?= $category["last_update"] ?></li>
</ul>
<a class="mx-10 bg-red-500 text-gray-50 px-4 py-2 my-2 rounded cursor-pointer" href="/Views/category.php?id=<?= $category["category_id"] ?>&remove">Supprimer</a>
<?php if(empty($films)): ?>
    <p class="px-10 py-20">Aucun film pour cette catégorie</p>
<?php else : ?>
    <table class="min-w-full divide-y divide-gray-200">
        <thead class="bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">#</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Titre</th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Durée</th>
                <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Actions</span>
                </th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
            <?php foreach ($films as $film) : ?>
                <tr>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["film_id"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["title"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        <?= $film["length"]; ?>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <a href="/Views/film.php?id=<?= $film["film_id"] ?>" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>


<?php
require("../Components/footer.html");
