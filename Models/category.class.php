<?php
require_once("../Models/pdo.class.php");

class CategoryRepository
{

    private $pdo = null;

    public function __construct()
    {
        $pdo = new PDO_SAKILA();
        $this->pdo = $pdo->getInstance();
    }

    function getAll()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM category");
        $stmt->execute();
        $categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $categories;
    }

    function get($id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM category WHERE category_id = :id");
        $stmt->execute([":id" => $id]);
        $category = $stmt->fetch(PDO::FETCH_ASSOC);

        return $category;
    }

    function getFilms($id)
    {
        $stmt = $this->pdo->prepare("SELECT f.* FROM film_category fc INNER JOIN film f ON f.film_id = fc.film_id WHERE category_id = :id");
        $stmt->execute([":id" => $id]);
        $films = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $films;
    }

    function add($name)
    {
        $stmt = $this->pdo->prepare("INSERT INTO category (name) VALUES(:name)");
        $ok = $stmt->execute([":name" => $name]);
        if ($ok) {
            header("Location: /Views/categories.php");
            exit();
        } else {
            die("Une erreur à eu lieu lors de l'insertion en base de donnée");
        }
    }

    function remove($id)
    {
        $stmt = $this->pdo->prepare("DELETE FROM category WHERE category_id = :id");
        $ok = $stmt->execute([":id" => $id]);
        if($ok){
            header("Location: /Views/categories.php");
            exit();
        } else {
            die("Une erreur à eu lieu lors de la suppression");
        }
    }
}
