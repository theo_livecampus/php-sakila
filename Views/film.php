<?php

require_once("../Models/film.class.php");

$filmId = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);

$filmRepository = new FilmRepository();
$film = $filmRepository->get($filmId);

$_title = $film["title"];

require_once("../Components/header.php");
require_once("../Components/navbar.html");
?>
<div class="flex justify-center py-5">
    <h1 class="text-3xl text-teal-700 font-bold"><?= $film["title"] ?></h1>
</div>
<ul class="flex justify-center text-gray-500 space-x-5">
    <li>ID : <?= $film["film_id"] ?></li>
    <li>Last update : <?= $film["last_update"] ?></li>
</ul>
<div class="px-20">

    <p class="my-5"><?= $film["description"] ?></p>
    <strong><?= $film["special_features"] ?></strong>
    <em><?= $film["rating"] ?></em>
    <h2 class="text-xl mt-5">Conditions de location : </h2>
    <ul class="px-5 mb-5">
        <li>Temps de location : <?= $film["rental_duration"] ?> jours</li>
        <li>Cout de location : <?= $film["rental_rate"] ?> €</li>
    </ul>
</div>


<?php
require("../Components/footer.html");
